package pl.sda.burger.service;

import pl.sda.burger.model.Order;
import pl.sda.burger.model.enums.Ingredient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

public class CurrentPlateHandler {
    private OrderValidator orderValidator;
    private Stack<Ingredient> ingredientsOnPlate = new Stack<>();

    public CurrentPlateHandler(OrderValidator orderValidator) {
        this.orderValidator = orderValidator;
    }

    public ArrayList<Ingredient> getIngredients() {
        return Collections.list(ingredientsOnPlate.elements());
    }

    public void addIngredient(Ingredient ingredient) {
        ingredientsOnPlate.add(ingredient);
    }

    public void clearPlate() {
        ingredientsOnPlate.clear();

    }

    public boolean validate(Order expectedIngredients) {
        return orderValidator.validate(ingredientsOnPlate, expectedIngredients);
    }

}
