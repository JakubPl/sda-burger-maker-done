package pl.sda.burger.service;

import pl.sda.burger.model.Order;
import pl.sda.burger.model.enums.Ingredient;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class OrderGenerator {

    private static final int MAXIMUM_NUMBER_OF_INGREDIENTS = 6;
    private static final int MINIMUM_NUMBER_OF_INGREDIENTS = 3;

    public Order generate() {
        final int ingredientsCount = Ingredient.values().length;
        List<Ingredient> ingredients = new LinkedList<>();
        do {
            final int randomIngredientIndex = new Random().nextInt(ingredientsCount - 1);
            final Ingredient randomIngredient = Ingredient.values()[randomIngredientIndex];
            ingredients.add(randomIngredient);
        } while (!isSizeBetweenMinAndMax(ingredients) || new Random().nextInt(5) == 4);
        return new Order(ingredients);
    }

    private boolean isSizeBetweenMinAndMax(List<Ingredient> ingredients) {
        return ingredients.size() < MAXIMUM_NUMBER_OF_INGREDIENTS && ingredients.size() > MINIMUM_NUMBER_OF_INGREDIENTS;
    }
}
