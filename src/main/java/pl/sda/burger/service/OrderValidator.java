package pl.sda.burger.service;

import pl.sda.burger.model.Order;
import pl.sda.burger.model.enums.Ingredient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class OrderValidator {
    public boolean validate(Stack<Ingredient> ingredientsOnPlate, Order orderToValidate) {
        List<Ingredient> expectedIngredients = orderToValidate.getIngredients();
        return ingredientsOnPlate.size() == expectedIngredients.size() && validateIngredients(ingredientsOnPlate, expectedIngredients);
    }

    private boolean validateIngredients(Stack<Ingredient> ingredientsOnPlate, List<Ingredient> expectedIngredients) {
        final ArrayList<Ingredient> ingredientsReversed = new ArrayList<>(expectedIngredients);
        Collections.reverse(ingredientsReversed);

        boolean allIngredientsCorrectAndInProperOrder = true;
        for (Ingredient expectedIngredient : ingredientsReversed) {
            final Ingredient nextIngredientFromTop = ingredientsOnPlate.pop();
            if (!expectedIngredient.equals(nextIngredientFromTop)) {
                allIngredientsCorrectAndInProperOrder = false;
                break;
            }
        }
        return allIngredientsCorrectAndInProperOrder;
    }
}
