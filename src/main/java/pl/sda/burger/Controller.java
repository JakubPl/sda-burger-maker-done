package pl.sda.burger;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import pl.sda.burger.model.Order;
import pl.sda.burger.model.enums.Ingredient;
import pl.sda.burger.service.CurrentPlateHandler;
import pl.sda.burger.service.OrderGenerator;
import pl.sda.burger.service.OrderValidator;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Controller {
    private static final int GAME_TIME = 60;
    private static final String EMPTY_PLATE = "Pusto";
    private static final String TIME_TOKEN = "Czas:";
    private static final String SCORE_TOKEN = "Wynik:";
    @FXML
    private Label score;
    @FXML
    private Label time;
    @FXML
    private Label order;
    @FXML
    private Label onPlate;

    private CurrentPlateHandler currentPlateHandler;
    private OrderGenerator orderGenerator;
    private OrderValidator orderValidator;
    private Order currentOrder;

    private int gameScore;
    private int gameTime;
    private ScheduledExecutorService scheduledExecutorService;
    private ScheduledFuture<?> timer;


    @FXML
    public void initialize() {
        this.gameTime = GAME_TIME;
        updateGameTimeOutsideMainThread();
        orderValidator = new OrderValidator();
        orderGenerator = new OrderGenerator();
        currentPlateHandler = new CurrentPlateHandler(orderValidator);
        generateOrder();
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        timer = scheduledExecutorService.scheduleAtFixedRate(this::decrementGameTimeOutsideMainThread, 1, 1, TimeUnit.SECONDS);
    }

    private void showScoreOutsideMainThread() {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Koniec gry");
            alert.setHeaderText("Gratulacje!");
            alert.setContentText(SCORE_TOKEN + gameScore);
            alert.showAndWait();
        });
    }

    public void sesameRollClicked(MouseEvent mouseEvent) {
        currentPlateHandler.addIngredient(Ingredient.SESAME_ROLL);
        updatePlateDisplay();
    }


    public void wheatRollClicked(MouseEvent mouseEvent) {
        currentPlateHandler.addIngredient(Ingredient.WHEAT_ROLL);
        updatePlateDisplay();
    }

    public void grahamRollClicked(MouseEvent mouseEvent) {
        currentPlateHandler.addIngredient(Ingredient.GRAHAM_ROLL);
        updatePlateDisplay();
    }

    public void meatClicked(MouseEvent mouseEvent) {
        currentPlateHandler.addIngredient(Ingredient.MEAT);
        updatePlateDisplay();
    }

    public void lettuceClicked(MouseEvent mouseEvent) {
        currentPlateHandler.addIngredient(Ingredient.LETTUCE);
        updatePlateDisplay();
    }

    public void tomatoClicked(MouseEvent mouseEvent) {
        currentPlateHandler.addIngredient(Ingredient.TOMATO);
        updatePlateDisplay();
    }

    public void cucumberClicked(MouseEvent mouseEvent) {
        currentPlateHandler.addIngredient(Ingredient.CUCUMBER);
        updatePlateDisplay();
    }

    public void pepperClicked(MouseEvent mouseEvent) {
        currentPlateHandler.addIngredient(Ingredient.PEPPER);
        updatePlateDisplay();
    }

    public void cheeseClicked(MouseEvent mouseEvent) {
        currentPlateHandler.addIngredient(Ingredient.CHEESE);
        updatePlateDisplay();
    }

    public void clearClicked(MouseEvent mouseEvent) {
        clearPlate();
    }

    public void readyClicked(MouseEvent mouseEvent) {
        if (currentPlateHandler.validate(currentOrder)) {
            gameScore++;
        } else {
            gameScore--;
        }
        currentPlateHandler.clearPlate();
        updatePlateDisplay();
        updateGameScore();
        generateOrder();
    }

    private void decrementGameTimeOutsideMainThread() {
        gameTime--;
        if (gameTime != -1) {
            updateGameTimeOutsideMainThread();
        } else {
            timer.cancel(true);
            showScoreOutsideMainThread();
        }
    }

    private void updateGameScore() {
        score.setText(SCORE_TOKEN + gameScore);
    }

    private void updateGameTimeOutsideMainThread() {
        Platform.runLater(() -> {
            time.setText(TIME_TOKEN + gameTime);
        });
    }

    private String ingredientListToReadable(List<Ingredient> ingredients) {
        return ingredients.stream()
                .map(Ingredient::getReadable)
                .collect(Collectors.joining("\n"));
    }

    private void updatePlateDisplay() {
        final String ingredientsToDisplay = ingredientListToReadable(currentPlateHandler.getIngredients());
        onPlate.setText(ingredientsToDisplay);
    }

    private void updateOrderDisplay() {
        final String ingredientsToDisplay = ingredientListToReadable(currentOrder.getIngredients());
        order.setText(ingredientsToDisplay);
    }

    private void generateOrder() {
        currentOrder = orderGenerator.generate();
        updateOrderDisplay();
    }

    private void clearPlate() {
        currentPlateHandler.clearPlate();
        onPlate.setText(EMPTY_PLATE);
    }

    public void shutdown() {
        scheduledExecutorService.shutdown();
    }
}
