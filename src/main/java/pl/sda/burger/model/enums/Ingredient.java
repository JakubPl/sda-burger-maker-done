package pl.sda.burger.model.enums;

public enum Ingredient {
    CUCUMBER("Ogorek"), LETTUCE("Salata"), TOMATO("Pomidor"), MEAT("Mieso"), PEPPER("Papryka"), CHEESE("Ser"), WHEAT_ROLL("Bulka pszenna"), SESAME_ROLL("Bulka z sezamem"), GRAHAM_ROLL("Bulka graham");

    private final String readable;

    Ingredient(String readable) {
        this.readable = readable;
    }

    public String getReadable() {
        return readable;
    }
}
